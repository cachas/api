const express = require('express');
const mysql = require('mysql');

const bodyParser = require('body-parser');

const PORT = process.env.PORT || 3050;

const app = express();

app.use(bodyParser.json());

//My sql
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'rootpass',
    database: 'pasiflora'
  });

// Check connect
connection.connect(error => {
    if (error) throw error;
    console.log('La conexión ha ido perfe');
})

app.listen(PORT, () => console.log(`Server running on port ${PORT}`)
)